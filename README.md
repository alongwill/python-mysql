# python-mysql

Example project using python 2.7 to implement database change control.

## Test Database

A test database can be provisioned using Docker as follows:

```bash
docker run --name mysql -e MYSQL_ROOT_PASSWORD=password -d -v /tmp/mysql:/var/lib/mysql -p 3306:3306 mysql:5.7
```

## Python setup

* Download and install [miniconda 2](https://docs.conda.io/en/latest/miniconda.html) (choose python 2.7 version)
* Create conda environment and install mysql-connector:

  ```bash
  conda create --name sandbox python=2.7
  conda activate sandbox
  # Install the latest version for Python 2.7
  conda install mysql-connector-python=8.0.18=py27h9c95fcb_1
  ```

## Usage

Tested on Ubuntu `18.04` with Docker `19.03.07`.

To process the existing files within `./changelog`:

```bash
./changelog.py changelog root localhost test password
```

Connect to the database to check the changelog is applied as expected:

```bash
docker exec -ti mysql mysql --password=password
...
mysql> use test;
mysql> select * from users;
+----+---------------------+----------+
| id | created_at          | username |
+----+---------------------+----------+
|  1 | 2020-03-07 13:32:07 | foo      |
|  2 | 2020-03-07 13:36:39 | bar      |
+----+---------------------+----------+
2 rows in set (0.00 sec)
```

## Tests

1. Confirm that files with ids before the current version are not processed.
  
   After initially processing the files in `./changelog` the latest version will be `20`.
   * Create a new file `./changelog/15.test.sql` with content

     ```sql
     INSERT INTO users (username) VALUES ('andy');
     ```

   * Run `./changelog.py` with the required arguments.
   * Querying the `users` table will show that `andy` has not been added.
  
2. Confirm that files with ids after the current version are processed.
   * Rename `./changelog/15.test.sql` to `./changelog/25.test.sql`
   * Run `./changelog.py` with the required arguments.
   * Querying the `users` table will show that `andy` has been added.
  