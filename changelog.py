#!/usr/bin/env python
from os import listdir
from os.path import isfile, join 
import re
import operator
import mysql.connector
import sys

def get_sorted_changelog_files(file_path):
    onlyfiles = [f for f in listdir(file_path) if isfile(join(file_path, f))]

    # Create a dict of numerical ids and their filenames
    file_dict = {}
    for file in onlyfiles:
        num = int(re.search(r'\d+', file).group())
        file_dict[num] = file

    sorted_dict = sorted(file_dict.items(), key=operator.itemgetter(0))

    return sorted_dict


def init_db(cnx,db_name):

    cursor = cnx.cursor()
    cursor.execute("CREATE DATABASE IF NOT EXISTS {} DEFAULT CHARACTER SET 'utf8'".format(db_name))
    cursor.execute("USE {}".format(db_name))
    cursor.execute("CREATE TABLE IF NOT EXISTS versionTable (version INT PRIMARY KEY)")
    cnx.commit()


def get_latest_version(cnx):
    cursor=cnx.cursor()
    cursor.execute("SELECT version FROM versionTable")
    rows = cursor.fetchall()
    if len(rows) == 1:
        (result,) = rows[0]
    else:
        # Initialise 
        cursor.execute("INSERT IGNORE INTO versionTable VALUES(0)")
        cnx.commit()
        result = 0
    print("Latest version is {}".format(result))
    return result

    
def process_files(cnx, latest_version, abs_path, sorted_files):
    cursor=cnx.cursor()
    for (id,filename) in sorted_files:
        if (id>latest_version):
            print("Processing {}".format(filename))
            with open(abs_path + '/' + filename, 'r') as file:
                data = file.read()
                cursor.execute(data)
            cursor.execute("UPDATE versionTable SET version = {};".format(id))
            cnx.commit()
            print("Updating version to {}".format(id))


def main():
    if len(sys.argv) < 6 or len(sys.argv) > 6:
        print """
        Usage: ./changelog.py <changelog_dir> <db_username> <db_host> <db_name> <db_password>
        """
        sys.exit(0)

    sql_dir     = sys.argv[1]
    db_username = sys.argv[2]
    db_host     = sys.argv[3]
    db_name     = sys.argv[4]
    db_password = sys.argv[5]

    cnx = mysql.connector.connect(
        host   = db_host,
        user   = db_username,
        passwd = db_password
    )
    
    init_db(cnx, db_name)
    # Set the default database to use
    cnx.database = db_name
    version = get_latest_version(cnx)

    sorted_files  = get_sorted_changelog_files(sql_dir)
    process_files(cnx, version, sql_dir, sorted_files)

main()